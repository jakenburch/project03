<?php
session_start();
  // including the db_connect file for database helper functions
  include 'db_connect.php';

  // opening the connection to the mysql database
  $mysql_link = connect('root', '', 'project3_db');

$survey_id = $_GET['id'];

  $survey_info = $mysql_link->query("
    SELECT name
    FROM survey
    WHERE id = '$survey_id'
  ");
  if($mysql_link->error) throw new \Exception($mysql_link->error);

$survey = $survey_info->fetch_assoc();

$questions = $mysql_link->query("
SELECT
  id,
  type,
  text 
  FROM question 
  WHERE survey_id = '$survey_id'
  ");

 if($mysql_link->error) throw new \Exception($mysql_link->error);

function allow_user_response($id, $type, $mysql_link){
if($type === "text"){
  $response = "<input type='text' name='question['$id'] />";
}else{
  $response = print_options_for($id, $type);

}
return $response;
}

function print_options_for($question_id, $type, $mysql_link){
$choices = $mysql_link->query("
SELECT 
name
FROM
choice 
WHERE question_id='$question_id'
  ");
 if($mysql_link->error) throw new \Exception($mysql_link->error);



 foreach($choices as $choice){
$choice_id = $choice['id'];
if($type === 'radio'){
$response .= "<input type='radio' name='question[$question_id]' value='$choice_id' />".$choice['name'];
}elseif($type === 'checkbox'){
$response .= "<input type='checkbox' name='question[$question_id][]' value='$choice_id'  />".$choice['name'];

}

 }
}
?><!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <h1><?= $survey['name'] ?></h1>
    <form action="answer-survey.php?id=<?= $survey_id ?>" method="POST">
      <ul>
    <?php foreach($questions as $question){ ?>
      <li><?=$question['text']?></li>
      <li><?= allow_user_response($question['id'], $question['type'], $mysql_link) ?></li>
    <?php } ?>
    </ul>
    <button type="submit">
        submit
    </button>
  </form>
  </body>
</html>
